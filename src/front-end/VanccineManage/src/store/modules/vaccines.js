// import { login, logout, getInfo } from '@/api/user'
import {getYiMiaoRecordList,postYiMiao,deleteVan,getPlanList,postPiCi,getSendist,postpostTheYimiaoPiCi } from '@/api/vaccines'

// import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  // token: getToken(),
  // name: '',
  // avatar: '',
  // introduction: '',
  // roles: []
}

const mutations = {
  // SET_TOKEN: (state, token) => {
  //   state.token = token
  // },
  // SET_INTRODUCTION: (state, introduction) => {
  //   state.introduction = introduction
  // },
  // SET_NAME: (state, name) => {
  //   state.name = name
  // },
  // SET_AVATAR: (state, avatar) => {
  //   state.avatar = avatar
  // },
  // SET_ROLES: (state, roles) => {
  //   state.roles = roles
  // }
}

const actions = {
  // user login
  // login({ commit }, userInfo) {
  //   const { username, password } = userInfo
  //   return new Promise((resolve, reject) => {
  //     login({ username: username.trim(), password: password }).then(response => {
  //       const { data } = response
  //       const token = 'xxxxxx'
  //       // const roleNames = ['admin']
  //       commit('SET_TOKEN', token)
  //       setToken(token)
  //       // setRole(roleNames)
  //       commit('SET_TOKEN', data.token)
  //       // setToken(data.token)
  //       resolve()
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // },

  // // get user info
  // getInfo({ commit, state }) {
  //   return new Promise((resolve, reject) => {
  //     getInfo(state.token).then(response => {
  //       const { data } = response

  //       if (!data) {
  //         reject('Verification failed, please Login again.')
  //       }

  //       const { roles, name, avatar, introduction } = data

  //       // roles must be a non-empty array
  //       if (!roles || roles.length <= 0) {
  //         reject('getInfo: roles must be a non-null array!')
  //       }

  //       commit('SET_ROLES', roles)
  //       commit('SET_NAME', name)
  //       commit('SET_AVATAR', avatar)
  //       commit('SET_INTRODUCTION', introduction)
  //       resolve(data)
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // },


// getYiMiaoRecordList
getPlanList({commit},paramObj) {
  debugger
  return new Promise((resolve,reject)=>{
    getPlanList(paramObj).then(res=>{
      debugger
      // const {data} =res;
      resolve(res.data);
    }).catch(err=>{
      reject(err)
    })
  })
},

  // 疫苗列表
  getYiMiaoRecordList({commit},paramObj) {
    debugger
    return new Promise((resolve,reject)=>{
      getYiMiaoRecordList(paramObj).then(res=>{
        debugger
        // const {data} =res;
        resolve(res.data);
      }).catch(err=>{
        reject(err)
      })
    })
  },

  // create
  postYiMiao({commit},paramObj){
    return new Promise((resolve,reject)=>{
      postYiMiao(paramObj).then(res=>{
        debugger
        // const {data} =res;
        resolve(res.data);
      }).catch(err=>{
        reject(err)
      })
    })
  },

  // 删除
  deleteVan({commit},id){
    debugger
    return new Promise((resolve,reject)=>{
      deleteVan(id).then(res=>{
        debugger
        // const {data} =res;
        resolve(res.data);
      }).catch(err=>{
        reject(err)
      })
    })
  },

// 创建批次
  postPiCi({commit},paramObj){
    return new Promise((resolve,reject)=>{
      postPiCi(paramObj).then(res=>{
        debugger
        // const {data} =res;
        resolve(res.data);
      }).catch(err=>{
        reject(err)
      })
    })
  },

// 疫苗下发
  postTheYimiao({commit},paramObj){
    return new Promise((resolve,reject)=>{
      postpostTheYimiaoPiCi(paramObj).then(res=>{
        debugger
        // const {data} =res;
        resolve(res.data);
      }).catch(err=>{
        reject(err)
      })
    })
  },


// 发放列表
getSendist({commit},paramObj) {
  debugger
  return new Promise((resolve,reject)=>{
    getSendist(paramObj).then(res=>{
      debugger
      // const {data} =res;
      resolve(res.data);
    }).catch(err=>{
      reject(err)
    })
  })
},

  // // user logout
  // logout({ commit, state, dispatch }) {
  //   return new Promise((resolve, reject) => {
  //     logout(state.token).then(() => {
  //       commit('SET_TOKEN', '')
  //       commit('SET_ROLES', [])
  //       removeToken()
  //       resetRouter()

  //       // reset visited views and cached views
  //       // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
  //       dispatch('tagsView/delAllViews', null, { root: true })

  //       resolve()
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // },

  // // remove token
  // resetToken({ commit }) {
  //   return new Promise(resolve => {
  //     commit('SET_TOKEN', '')
  //     commit('SET_ROLES', [])
  //     removeToken()
  //     resolve()
  //   })
  // },

  // // dynamically modify permissions
  // async changeRoles({ commit, dispatch }, role) {
  //   const token = role + '-token'

  //   commit('SET_TOKEN', token)
  //   setToken(token)

  //   const { roles } = await dispatch('getInfo')

  //   resetRouter()

  //   // generate accessible routes map based on roles
  //   const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })
  //   // dynamically add accessible routes
  //   router.addRoutes(accessRoutes)

  //   // reset visited views and cached views
  //   dispatch('tagsView/delAllViews', null, { root: true })
  // }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
