import request from '@/utils/request'

// 列表
export function getYiMiaoRecordList(data) {
  return request({
    url: 'Gwx.Xncf.Vaccines/VaccineAppService/Xncf.Vaccines_VaccineAppService.Get',
    method: 'get',
    params:{data}
  })
}

// 创建
export function postYiMiao(data) {
  return request({
    url: 'Gwx.Xncf.Vaccines/VaccineAppService/Xncf.Vaccines_VaccineAppService.Create',
    method: 'post',
    data
  })
}

// 删除
export function deleteVan(id) {
  return request({
    url: `Gwx.Xncf.Vaccines/VaccineAppService/Xncf.Vaccines_VaccineAppService.Delete`,
    method: 'delete',
    data:id
  })
}

// 批次列表
export function getPlanList(id) {
  return request({
    url: `Gwx.Xncf.Vaccines/BatchVaccineAppService/Xncf.Vaccines_BatchVaccineAppService.Get`,
    method: 'get',
    params:{data}
  })
}

// 发放列表

export function getSendist(data) {
  return request({
    url: `Gwx.Xncf.Vaccines/PlanVaccineAppService/Xncf.Vaccines_PlanVaccineAppService.Get`,
    method: 'get',
    params:{data}
  })
}

// 创建批次

export function postPiCi(data) {
  return request({
    url: `Gwx.Xncf.Vaccines/BatchVaccineAppService/Xncf.Vaccines_BatchVaccineAppService.Create`,
    method: 'post',
    data
  })
}


// 创建下发
export function postpostTheYimiaoPiCi(data) {
  return request({
    url: `Gwx.Xncf.Vaccines/PlanVaccineAppService/Xncf.Vaccines_PlanVaccineAppService.Create`,
    method: 'post',
    data
  })
}

