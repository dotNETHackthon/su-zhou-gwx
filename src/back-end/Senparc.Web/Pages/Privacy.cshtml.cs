﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Senparc.Web.Models.VD;
using Senparc.Weixin.Entities;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;

namespace Senparc.Web.Pages
{
    public class PrivacyModel : BasePageModel
    {
        private readonly SenparcWeixinSetting _senparcWeixinSetting;

        public PrivacyModel(IOptions<SenparcWeixinSetting> weixinSetting)
        {
            _senparcWeixinSetting = weixinSetting.Value;
        }

        public string ReturnUrl { get; set; }

        public void OnGet()
        {
            var redirectUrl = "http://348e-240e-3a3-4c00-19f0-b5af-f941-5454-7f95.ngrok.io/client";
            ReturnUrl =
                $"https://open.weixin.qq.com/connect/oauth2/authorize?appid={_senparcWeixinSetting.MpSetting.WeixinAppId}&redirect_uri={redirectUrl}&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
        }

        // public ActionResult Callback(string code, string state, string returnUrl)
        // {
        //     OAuthAccessTokenResult result = null;
        //     try
        //     {
        //         result = OAuthApi.GetAccessToken( _senparcWeixinSetting.MpSetting.WeixinAppId, _senparcWeixinSetting.MpSetting.WeixinAppSecret, code);
        //     }
        //     catch (Exception ex)
        //     {
        //         return Content(ex.Message);
        //     }
        //     OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
        //     return View(userInfo);
        // }
    }
}