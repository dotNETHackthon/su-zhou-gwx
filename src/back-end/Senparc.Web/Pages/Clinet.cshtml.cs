using GWX.Xncf.Reservation;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using GWX.Xncf.Reservation.OHS.Local.AppService;
using GWX.Xncf.Reservation.OHS.Local.PL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Senparc.Ncf.Core.Models;
using Senparc.Web.Models.VD;
using Senparc.Weixin.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Senparc.Web.Pages
{
    public class ClinetModel : BasePageModel
    {
        ClientAppService _clientService;
        private readonly SenparcWeixinSetting _senparcWeixinSetting;
        public PagedList<Reservation> list { get; set; }
        public string OpenId { get; set; }
        public string HeadImg { get; set; }


        public ClinetModel(ClientAppService clientService, IOptions<SenparcWeixinSetting> weixinSetting)
        {
            _clientService = clientService;
            _senparcWeixinSetting = weixinSetting.Value;
        }
        public void OnGet()
        {
            var OpenId = HttpContext.Session.GetString("OpenId");
            //var OpenId = "456";
            if (!string.IsNullOrEmpty(OpenId))
            {
                Reservation_GetListRequest request = new Reservation_GetListRequest();
                request.OpenId = OpenId;
                var data = _clientService.GetAllList(request);
                list = data;//预约列表
                HeadImg = HttpContext.Session.GetString("HeadImg");
            }
            else
            {
                var redirectUrl = "http://348e-240e-3a3-4c00-19f0-b5af-f941-5454-7f95.ngrok.io/WxCallback";//处理微信回调的页面
                var appId = _senparcWeixinSetting.WeixinAppId;
                var ReturnUrl = string.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope=SCOPE&state=STATE#wechat_redirect", appId, redirectUrl);
                Redirect(ReturnUrl);
            }
            
        }
    }
}
