using Gwx.Xncf.Vaccines.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Web.Models.VD;

namespace Senparc.Web.Pages;

public class Dashboard : BasePageModel
{
    private readonly VaccineService _vaccineService;
    private readonly PlanVaccineService _planVaccineService;
    private readonly BatchVaccineService _batchVaccineService;

    public Dashboard(VaccineService vaccineService, PlanVaccineService planVaccineService,
        BatchVaccineService batchVaccineService)
    {
        _vaccineService = vaccineService;
        _planVaccineService = planVaccineService;
        _batchVaccineService = batchVaccineService;
    }

    public IActionResult OnGet()
    {
        var vaccine = _vaccineService.GetCount(z => true);
        var planVaccine = _planVaccineService.GetCount(z => true);
        var batchVaccine = _batchVaccineService.GetCount(z => true);
        return new JsonResult(new {vaccine, planVaccine, batchVaccine});
    }
}