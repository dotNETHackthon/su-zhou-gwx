using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Senparc.NeuChar.App.AppStore;
using Senparc.Weixin.Entities;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;


namespace Senparc.Web.Pages
{
    public class WxCallbackModel : PageModel
    {
        private readonly SenparcWeixinSetting _senparcWeixinSetting;

        public WxCallbackModel(IOptions<SenparcWeixinSetting> weixinSetting)
        {
            _senparcWeixinSetting = weixinSetting.Value;
        }
        public void OnGet(string code)
        {

            try
            {
                Weixin.MP.AdvancedAPIs.OAuth.OAuthAccessTokenResult result = OAuthApi.GetAccessToken(_senparcWeixinSetting.MpSetting.WeixinAppId, _senparcWeixinSetting.MpSetting.WeixinAppSecret, code);
                OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                HttpContext.Session.SetString("OpenId", userInfo.openid);
                HttpContext.Session.SetString("HeadImg", userInfo.headimgurl);
            }
            catch (Exception ex)
            {
         
            }
            

        }
    }
}
