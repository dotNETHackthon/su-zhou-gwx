﻿//以下数据库模块的命名空间根据需要添加或删除
using Microsoft.Extensions.DependencyInjection;
using Senparc.CO2NET.AspNet;
using Senparc.Ncf.Database.MySql;//使用需要引用包： Senparc.Ncf.Database.MySql
//using Senparc.Ncf.Database.Sqlite;//使用需要引用包： Senparc.Ncf.Database.Sqlite
//using Senparc.Ncf.Database.PostgreSQL;//使用需要引用包： Senparc.Ncf.Database.PostgreSQL
using Senparc.Ncf.Database.SqlServer;//使用需要引用包： Senparc.Ncf.Database.SqlServer

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
//添加（注册） Ncf 服务（必须）
builder.AddNcf<MySqlDatabaseConfiguration>();

builder.Services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                   builder =>
                   {
                       builder.WithOrigins(
                           "http://localhost:9527",
                           "http://192.168.1.10:9527"
                           )
                       .AllowAnyMethod()
                       .AllowCredentials()
                       .AllowAnyHeader();
                   });
            });

#region AddNcf<TDatabaseConfiguration>() 泛型类型说明
/* 
 *                  方法                            |         说明
 * -------------------------------------------------|-------------------------
 *  AddNcf<SQLServerDatabaseConfiguration>()        |  使用 SQLServer 数据库
 *  AddNcf<SqliteMemoryDatabaseConfiguration>()     |  使用 SQLite 数据库
 *  AddNcf<MySqlDatabaseConfiguration>()            |  使用 MySQL 数据库
 *  AddNcf<PostgreSQLDatabaseConfiguration>()       |  使用 PostgreSQL 数据库
 *  更多数据库可扩展，依次类推……
 *  
 */
#endregion

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

//Use NCF（必须）
app.UseNcf();

// app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseCookiePolicy();

app.UseCors();
app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapRazorPages();
    endpoints.MapControllers();
});

app.Run();
