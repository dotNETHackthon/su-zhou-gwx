﻿using AutoMapper;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;


namespace GWX.Xncf.Reservation.AutoMpperProfiles
{
    public class SenparcAreaAdminAutoMapperProfile: Profile
    {
        public SenparcAreaAdminAutoMapperProfile()
        {
            CreateMap<ReservationDto, Reservation>();

        }
    }
}
