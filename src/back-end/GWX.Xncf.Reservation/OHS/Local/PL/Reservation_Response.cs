﻿using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Reservation.OHS.Local.PL
{
    public class Reservation_Response
    {
    }
    /// <summary>
    /// 列表
    /// </summary>
    public class Reservation_GetListResponse
    {
        public IEnumerable<ReservationDto> List { get; set; }

        public int TotalCount { get; set; }
    }
}
