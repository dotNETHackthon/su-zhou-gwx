﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Reservation.OHS.Local.PL
{
    public class Reservation_Request
    {
    }
    public class Reservation_GetListRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public DateTime? AppointmentSatrtDate { get; set; }

        public DateTime? AppointmentEndDate { get; set; }

        public string AppointmentorName { get; set; }

        public string OpenId { get; set; }
    }
}
