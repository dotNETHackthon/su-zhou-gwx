﻿using Senparc.Ncf.Core.AppServices;
using GWX.Xncf.Reservation.Domain.Services;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Senparc.CO2NET;
using Senparc.CO2NET.Extensions;
using GWX.Xncf.Reservation.OHS.Local.PL;
using Senparc.Ncf.Utility;
using System.Linq;
using Senparc.Ncf.Core.Models;

namespace GWX.Xncf.Reservation.OHS.Local.AppService
{
    public class ClientAppService : AppServiceBase
    {
        private ClientService _reservationService;
        private readonly AutoMapper.IMapper _mapper;
        public ClientAppService(IServiceProvider serviceProvider, ClientService reservationService, AutoMapper.IMapper mapper) : base(serviceProvider)
        {
            _reservationService = reservationService;
            _mapper = mapper;
        }

        /// <summary>
        /// 创建一个新预约
        /// </summary>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<ReservationDto> CreateNewReservationAsync(ReservationDto request)
        {

            var reservationDto = await _reservationService.CreateNewReservationAsync(request).ConfigureAwait(false);
            return reservationDto;

        }

        /// <summary>
        /// 获取分页列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        //[Permission("role.query")]
        public async Task<AppResponseBase<Reservation_GetListResponse>> GetListAsync(Reservation_GetListRequest request)
        {
            var response = await this.GetResponseAsync<AppResponseBase<Reservation_GetListResponse>, Reservation_GetListResponse>(async (response, logger) =>
            {
                SenparcExpressionHelper<Reservation> helper = new SenparcExpressionHelper<Reservation>();
                helper.ValueCompare.AndAlso(!string.IsNullOrEmpty(request.OpenId), s => s.OpenId == request.OpenId);
                helper.ValueCompare.AndAlso(!string.IsNullOrEmpty(request.AppointmentorName), s => s.AppointmentorName.Contains(request.AppointmentorName));
                if (request.AppointmentSatrtDate != null)
                {
                    helper.ValueCompare.AndAlso(true, s => s.AppointmentDate < request.AppointmentSatrtDate);
                }
                if (request.AppointmentEndDate != null)
                {
                    helper.ValueCompare.AndAlso(true, s => s.AppointmentDate > request.AppointmentEndDate);
                }
                var count = _reservationService.GetCount(z => true);
                var list = await _reservationService.GetObjectListAsync(request.PageIndex, request.PageSize, helper.BuildWhereExpression(), _ => _.AddTime, Senparc.Ncf.Core.Enums.OrderingType.Descending);

                var dtoItems = _mapper.Map<IEnumerable<ReservationDto>>(list.AsEnumerable());
                return new Reservation_GetListResponse()
                {
                    TotalCount = list.TotalCount,
                    List = dtoItems
                };
            });
            return response;
        }
        /// <summary>
        /// 依据Id获取预约信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<ReservationDto> GetReservationById(int Id)
        {
            var data = await _reservationService.GetObjectAsync(s => s.Id == Id);
            var result = _mapper.Map<ReservationDto>(data);
            return result;
        }

        public  PagedList<Reservation> GetAllList(Reservation_GetListRequest request)
        {
            SenparcExpressionHelper<Reservation> helper = new SenparcExpressionHelper<Reservation>();
            helper.ValueCompare.AndAlso(!string.IsNullOrEmpty(request.OpenId), s => s.OpenId == request.OpenId);
            helper.ValueCompare.AndAlso(!string.IsNullOrEmpty(request.AppointmentorName), s => s.AppointmentorName.Contains(request.AppointmentorName));
            if (request.AppointmentSatrtDate != null)
            {
                helper.ValueCompare.AndAlso(true, s => s.AppointmentDate < request.AppointmentSatrtDate);
            }
            if (request.AppointmentEndDate != null)
            {
                helper.ValueCompare.AndAlso(true, s => s.AppointmentDate > request.AppointmentEndDate);
            }
            var count = _reservationService.GetCount(z => true);

            var list =  _reservationService.GetObjectList(1, 999, helper.BuildWhereExpression(), s => s.AddTime, Senparc.Ncf.Core.Enums.OrderingType.Descending);

            return list;
        }
    }
}
