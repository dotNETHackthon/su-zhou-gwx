﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.XncfBase;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

using GWX.Xncf.Reservation.Models;
using GWX.Xncf.Reservation.OHS.Local.AppService;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Database;
using Senparc.Ncf.XncfBase.Database;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using AutoMapper;

namespace GWX.Xncf.Reservation
{
    [XncfRegister]
    public partial class Register : XncfRegisterBase, IXncfRegister
    {
        #region IXncfRegister 接口

        public override string Name => "GWX.Xncf.Reservation";

        public override string Uid => "D3075481-9A6E-4264-9F1D-B77D21FFC186";//必须确保全局唯一，生成后必须固定，已自动生成，也可自行修改

        public override string Version => "1.1";//必须填写版本号

        public override string MenuName => "预约管理";

        public override string Icon => "fa fa-star";

        public override string Description => "预约模块";

        public override async Task InstallOrUpdateAsync(IServiceProvider serviceProvider, InstallOrUpdate installOrUpdate)
        {
            //安装或升级版本时更新数据库
            await XncfDatabaseDbContext.MigrateOnInstallAsync(serviceProvider, this);

            //根据安装或更新不同条件执行逻辑
            switch (installOrUpdate)
            {
                case InstallOrUpdate.Install:
                    //新安装
            #region 初始化数据库数据
                    //var colorService = serviceProvider.GetService<ClientAppService>();
                    //ColorDto color = await colorService.GetOrInitColorDtoAsync();
            #endregion
                    break;
                case InstallOrUpdate.Update:
                    //更新
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override async Task UninstallAsync(IServiceProvider serviceProvider, Func<Task> unsinstallFunc)
        {
            #region 删除数据库（演示）

            var mySenparcEntitiesType = this.TryGetXncfDatabaseDbContextType;
            ReservationSenparcEntities mySenparcEntities = serviceProvider.GetService(mySenparcEntitiesType) as ReservationSenparcEntities;

            //指定需要删除的数据实体

            //注意：这里作为演示，在卸载模块的时候删除了所有本模块创建的表，实际操作过程中，请谨慎操作，并且按照删除顺序对实体进行排序！
            var dropTableKeys = EntitySetKeys.GetEntitySetInfo(this.TryGetXncfDatabaseDbContextType).Keys.ToArray();
            await base.DropTablesAsync(serviceProvider, mySenparcEntities, dropTableKeys);

            #endregion
            await unsinstallFunc().ConfigureAwait(false);
        }
        #endregion

        public override IServiceCollection AddXncfModule(IServiceCollection services, IConfiguration configuration, IHostEnvironment env)
        {
            services.AddScoped<ClientAppService>();
            return base.AddXncfModule(services, configuration, env);
        }
        public override void OnAutoMapMapping(IServiceCollection services, IConfiguration configuration)
        {
            base.OnAutoMapMapping(services, configuration);
            services.AddAutoMapper(z => z.AddProfile<AutoMpperProfiles.SenparcAreaAdminAutoMapperProfile>());
        }
    }
}
