﻿using Senparc.Ncf.Core.Models;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GWX.Xncf.Reservation
{
    /// <summary>
    /// Color 实体类
    /// </summary>
    [Table(Register.DATABASE_PREFIX + nameof(Reservation))]//必须添加前缀，防止全系统中发生冲突
    [Serializable]
    public class Reservation : EntityBase<int>
    {
        /// <summary>
        /// 预约人姓名
        /// </summary>
        public string AppointmentorName { get; private set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IdNo { get; private set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; private set; }

        /// <summary>
        /// 过敏史
        /// </summary>
        public string AllergicHistory { get; private set; }

        /// <summary>
        /// 预约日期
        /// </summary>
        public DateTime? AppointmentDate { get; private set; }

        /// <summary>
        /// 预约状态:0：预约中   1：已接种    2：已过期
        /// </summary>
        public int? Status { get; private set; }

        /// <summary>
        /// 计划ID
        /// </summary>
        public int? PlanId { get; private set; }

        /// <summary>
        /// 计划名称
        /// </summary>
        public string PlanName { get; private set; }

        /// <summary>
        /// 接种点ID
        /// </summary>
        public int? InoculationpointId { get; private set; }

        /// <summary>
        /// 接种点名称
        /// </summary>
        public string InoculationpointName { get; private set; }

        /// <summary>
        /// 批次Id
        /// </summary>
        public int BatchId { get; private set; }

        /// <summary>
        /// 批次编号
        /// </summary>
        public string BatchNo { get; private set; }

        public string OpenId { get; private set; }

        public Reservation() { }

        public Reservation(string appointmentorName, string idNo, string sex, string allergicHistory, DateTime? appointmentDate, int? status, int? planId, string planName, int? inoculationpointId, string inoculationpointName, int batchId, string batchNo, string openId)
        {
            AppointmentorName = appointmentorName;
            IdNo = idNo;
            Sex = sex;
            AllergicHistory = allergicHistory;
            AppointmentDate = appointmentDate;
            Status = status;
            PlanId = planId;
            PlanName = planName;
            InoculationpointId = inoculationpointId;
            InoculationpointName = inoculationpointName;
            BatchId = batchId;
            BatchNo = batchNo;
            OpenId = openId;
        }

        public Reservation(ReservationDto reservationDto)
        {
            AppointmentorName = reservationDto.AppointmentorName;
            IdNo = reservationDto.IdNo;
            Sex = reservationDto.Sex;
            AllergicHistory = reservationDto.AllergicHistory;
            AppointmentDate = reservationDto.AppointmentDate;
            Status = reservationDto.Status;
            PlanId = reservationDto.PlanId;
            PlanName = reservationDto.PlanName;
            InoculationpointId = reservationDto.InoculationpointId;
            InoculationpointName = reservationDto.InoculationpointName;
            BatchId = reservationDto.BatchId;
            OpenId = reservationDto.OpenId;
        }

        public void SetReservationStatus(int? status)
        {
            Status = status;
        }
    }
}
