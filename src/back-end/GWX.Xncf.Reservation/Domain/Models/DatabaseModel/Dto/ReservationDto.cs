﻿using Senparc.Ncf.Core.Models;
using System;


namespace GWX.Xncf.Reservation.Models.DatabaseModel.Dto
{
    public class ReservationDto : DtoBase
    {
        /// <summary>
        /// 预约人姓名
        /// </summary>
        public string AppointmentorName { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IdNo { get;  set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get;  set; }

        /// <summary>
        /// 过敏史
        /// </summary>
        public string AllergicHistory { get;  set; }

        /// <summary>
        /// 预约日期
        /// </summary>
        public DateTime? AppointmentDate { get;  set; }

        /// <summary>
        /// 预约状态:0：预约中   1：已接种    2：已过期
        /// </summary>
        public int? Status { get;  set; }

        /// <summary>
        /// 计划ID
        /// </summary>
        public int? PlanId { get;  set; }

        /// <summary>
        /// 计划名称
        /// </summary>
        public string PlanName { get;  set; }

        /// <summary>
        /// 接种点ID
        /// </summary>
        public int? InoculationpointId { get;  set; }

        /// <summary>
        /// 接种点名称
        /// </summary>
        public string InoculationpointName { get;  set; }

        /// <summary>
        /// 批次Id
        /// </summary>
        public int BatchId { get;  set; }

        /// <summary>
        /// 批次编号
        /// </summary>
        public string BatchNo { get;  set; }

        public string OpenId { get;  set; }

        /// <summary>
        /// 冗余疫苗供应商
        /// </summary>
        [AutoMapper.IgnoreMap]
        public string SupplierName { get; set; }

        /// <summary>
        /// 冗余疫苗类别
        /// </summary>
        /// 
        [AutoMapper.IgnoreMap]
        public string VaccinesType { get; set; }

        

        public ReservationDto() { }



    }
}
