﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using GWX.Xncf.Reservation.Domain.Services;
using GWX.Xncf.Reservation.Models.DatabaseModel.Dto;
using System;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Gwx.Xncf.Vaccines.Domain.Services;
using Gwx.Xncf.Vaccines.OHS.Local.AppService;

namespace GWX.Xncf.Reservation.Domain.Services
{
    public class ClientService : ServiceBase<Reservation>
    {
        private readonly BatchVaccineAppService _batchVaccineService;
        public ClientService(IRepositoryBase<Reservation> repo, IServiceProvider serviceProvider, BatchVaccineAppService batchVaccineService)
            : base(repo, serviceProvider)
        {
            _batchVaccineService = batchVaccineService;
        }

        public async Task<ReservationDto> CreateNewReservationAsync(ReservationDto request)
        {
            var data = base.Mapper.Map<Reservation>(request);
            data.SetReservationStatus(0);//0：预约中   1：已接种    2：已过期
            var vaccine = await _batchVaccineService.GetBatchVaccineById(data.BatchId);
            if (vaccine == null)
            {
                throw new Senparc.Ncf.Core.Exceptions.NcfExceptionBase("疫苗批次不存在");
            }
            if (vaccine.Qty <= 0)
            {
                throw new Senparc.Ncf.Core.Exceptions.NcfExceptionBase("疫苗批次库存不足");
            }
            await base.SaveObjectAsync(data).ConfigureAwait(false);
            return request;
        }
        
        //TODO: 更多业务方法可以写到这里
    }
}
