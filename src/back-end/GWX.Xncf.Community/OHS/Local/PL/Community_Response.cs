﻿using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.OHS.Local.PL
{
    /// <summary>
    /// 返回参数
    /// </summary>
    public class Community_Response
    {
        /// <summary>
        /// 社区名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress
        {
            get;
            set;
        }
    }
    public class Community_GetListResponse
    {
        
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }
        public List<CommunityDto> List { get; set; }
        public int TotalCount { get; set; }
    }
}

