﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.OHS.Local.PL
{
    public class Community_Request
    {
        /// <summary>
        /// 社区名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress
        {
            get;
            set;
        }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }
        public int Id { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Search { get; set; }
    }

    /// <summary>
    /// 添加社区
    /// </summary>
    public class Community_CreateRequest
    {
        /// <summary>
        /// 社区名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress
        {
            get;
            set;
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }
    }
}
