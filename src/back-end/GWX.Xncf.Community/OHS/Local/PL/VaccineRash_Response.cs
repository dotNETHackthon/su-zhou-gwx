﻿using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using System.Collections.Generic;

namespace GWX.Xncf.Community.OHS.Local.PL
{
    /// <summary>
    /// 返回参数
    /// </summary>
    public class VaccineRash_Response
    {
        /// <summary>
        /// 接种点名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address
        {
            get;
            set;
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone
        {
            get;
            set;
        }

        /// <summary>
        /// 社区id
        /// </summary>
        public int CommunityId
        {
            get;
            set;
        }

        /// <summary>
        /// 社区名称
        /// </summary>
        public string CommunityName
        {
            get;
            set;
        }
    }

    public class VaccineRash_GetListResponse
    {
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

        public List<VaccineRashDto> List { get; set; }
        public int TotalCount { get; set; }
    }
}