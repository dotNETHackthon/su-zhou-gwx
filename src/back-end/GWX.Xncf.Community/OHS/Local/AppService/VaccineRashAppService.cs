﻿using AutoMapper;
using GWX.Xncf.Community.Domain.Models.DatabaseModel;
using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using GWX.Xncf.Community.Domain.Services;
using GWX.Xncf.Community.OHS.Local.PL;
using Senparc.CO2NET;
using Senparc.CO2NET.Extensions;
using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.OHS.Local.AppService
{
    /// <summary>
    /// 接种点服务
    /// </summary>
    public class VaccineRashAppService : AppServiceBase
    {
        private VaccineRashService _vaccineRashService;
        private IMapper _mapper;
        public VaccineRashAppService(IServiceProvider serviceProvider, VaccineRashService vaccineRashService, IMapper mapper) : base(serviceProvider)
        {
            this._vaccineRashService = vaccineRashService;
            this._mapper = mapper;
        }
        /// <summary>
        /// 添加接种点信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<VaccineRash_Response>> CreateOrUpdate(VaccineRash_Request request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<VaccineRash_Response>, VaccineRash_Response>(async (response, logger) =>
            {
                var dto = _mapper.Map<VaccineRashDto>(request);
                await _vaccineRashService.CreateAsync(dto);
                return new VaccineRash_Response();
            });
            return result;
        }

        /// <summary>
        /// 删除接种点
        /// </summary>
        /// <param name="request">接种点信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Delete)]
        public async Task<AppResponseBase<bool>> Delete(int id)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _vaccineRashService.DeleteByIdAsync(id);
                return result;
            });
            return result;
        }

        /// <summary>
        /// 查询所有接种点和社区信息
        /// </summary>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">大小 </param>
        /// <param name="search">关键词</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<VaccineRash_GetListResponse>> Get(int pageIndex, int pageSize = 10, string search = "")
        {
            var result = await this.GetResponseAsync<AppResponseBase<VaccineRash_GetListResponse>, VaccineRash_GetListResponse>(async (response, logger) =>
            {
                var seh = new SenparcExpressionHelper<VaccineRash>();
                seh.ValueCompare.AndAlso(!search.IsNullOrEmpty(), z => z.Name.Contains(search));
                var where = seh.BuildWhereExpression();

                //关联查询
                var list = await _vaccineRashService.GetObjectListAsync(pageIndex, pageSize, where, z => z.Id, Senparc.Ncf.Core.Enums.OrderingType.Descending);
                var dtoCommunitys = _mapper.Map<List<VaccineRashDto>>(list);
                
                return new VaccineRash_GetListResponse()
                {
                    List = dtoCommunitys,
                    TotalCount = list.TotalCount
                };
            });
            return result;
        }
        
    }
}
