﻿using AutoMapper;
using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using GWX.Xncf.Community.Domain.Services;
using GWX.Xncf.Community.OHS.Local.PL;
using Senparc.CO2NET;
using Senparc.CO2NET.Extensions;
using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.OHS.Local.AppService
{
    /// <summary>
    /// 社区服务
    /// </summary>
    public class CommunityAppService : AppServiceBase
    {
        private CommunityService _communityService;
        private IMapper _mapper;

        public CommunityAppService(CommunityService communityService, IServiceProvider serviceProvider, IMapper mapper) : base(serviceProvider)
        {
            _communityService = communityService;
            _mapper = mapper;
        }
        /// <summary>
        /// 添加疫苗信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<Community_Response>> CreateOrUpdate(Community_Request request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<Community_Response>, Community_Response>(async (response, logger) =>
            { 
                var dto = _mapper.Map<CommunityDto>(request);
                await _communityService.CreateOrUpdateAsync(dto);
                return new Community_Response();
            });
            return result;
        }

        /// <summary>
        /// 添加社区信息
        /// </summary>
        /// <param name="request">社区信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<bool>> Create(Community_CreateRequest request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                await _communityService.CreateOrUpdateAsync(new CommunityDto() { Name = request.Name, Phone = request.Phone, DetailedAddress = request.DetailedAddress });
                return true;
            });
            return result;
        }

        /// <summary>
        /// 编辑社区信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Put)]
        public async Task<AppResponseBase<bool>> Update(int id, Community_CreateRequest request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                await _communityService.CreateOrUpdateAsync(new CommunityDto() { Id = id, Name = request.Name, Phone = request.Phone, DetailedAddress = request.DetailedAddress });
                return true;
            });
            return result;
        }

        /// <summary>
        /// 删除社区
        /// </summary>
        /// <param name="request">社区信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Delete)]
        public async Task<AppResponseBase<bool>> Delete(int id)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _communityService.DeleteByIdAsync(id);
                return result;
            });
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">大小 </param>
        /// <param name="search">关键词</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<Community_GetListResponse>> Get(int pageIndex, int pageSize = 10, string search = "")
        {
            var result = await this.GetResponseAsync<AppResponseBase<Community_GetListResponse>, Community_GetListResponse>(async (response, logger) =>
            {
                var seh = new SenparcExpressionHelper<Community.Domain.Models.DatabaseModel.Community>();
                seh.ValueCompare.AndAlso(!search.IsNullOrEmpty(), z => z.Name.Contains(search));
                var where = seh.BuildWhereExpression();

                var list = await _communityService.GetObjectListAsync(pageIndex, pageSize, where, z => z.Id, Senparc.Ncf.Core.Enums.OrderingType.Descending);
                var dtoCommunitys = _mapper.Map<List<CommunityDto>>(list);
                return new Community_GetListResponse()
                {
                    List = dtoCommunitys,
                    TotalCount = list.TotalCount
                };
            });
            return result;
        }
        /// <summary>
        /// 删除疫苗
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Delete)]
        public async Task<AppResponseBase<bool>> Delete(Community_Request request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _communityService.DeleteByIdAsync(request.Id);
                return true;
            });
            return result;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<Community_GetListResponse>> Get(Community_Request request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<Community_GetListResponse>, Community_GetListResponse>(async (response, logger) =>
            {
                var list = await _communityService.GetObjectListAsync(request.PageIndex, request.PageSize, z => true, z => z.Id, Senparc.Ncf.Core.Enums.OrderingType.Descending);
                var dtoCommunity = _mapper.Map<List<CommunityDto>>(list);
                return new Community_GetListResponse()
                {
                    List = dtoCommunity,
                    TotalCount = list.TotalCount
                };
            });
            return result;
        }
    }
}
