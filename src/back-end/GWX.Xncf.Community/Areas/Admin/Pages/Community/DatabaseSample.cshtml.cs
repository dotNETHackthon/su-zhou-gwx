﻿using Microsoft.AspNetCore.Mvc;
using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using Senparc.Ncf.Core.Models;

namespace GWX.Xncf.Community.Areas.Community.Pages
{
    public class DatabaseSample : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
    {

        public MultipleDatabaseType MultipleDatabaseType { get; set; }

        private readonly IServiceProvider _serviceProvider;
        public DatabaseSample(IServiceProvider serviceProvider, Lazy<XncfModuleService> xncfModuleService)
            : base(xncfModuleService)
        {
            _serviceProvider = serviceProvider;

            var databaseConfigurationFactory = DatabaseConfigurationFactory.Instance;
            var currentDatabaseConfiguration = databaseConfigurationFactory.Current;
            MultipleDatabaseType = currentDatabaseConfiguration.MultipleDatabaseType;
        }
    }
}
