﻿using Microsoft.EntityFrameworkCore;
using Senparc.Ncf.Database;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.XncfBase.Database;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel
{
    public class CommunitySenparcEntities : XncfDatabaseDbContext
    {
        public CommunitySenparcEntities(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
        }
        /// <summary>
        /// 疫苗
        /// </summary>
        public DbSet<Community> Communitys { get; set; }

        /// <summary>
        /// 批次记录
        /// </summary>
        public DbSet<VaccineRash> VaccineRashs { get; set; }

        //DOT REMOVE OR MODIFY THIS LINE 请勿移除或修改本行 - Entities Point
        //ex. public DbSet<Color> Colors { get; set; }

        //如无特殊需需要，OnModelCreating 方法可以不用写，已经在 Register 中要求注册
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //}
    }
}
