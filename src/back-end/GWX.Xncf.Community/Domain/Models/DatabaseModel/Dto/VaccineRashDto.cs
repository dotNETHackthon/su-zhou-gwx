﻿using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto
{
    public class VaccineRashDto : DtoBase
    {
        /// <summary>
        /// 接种点名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address
        {
            get;
            set;
        }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone
        {
            get;
            set;
        }
        /// <summary>
        /// 社区id
        /// </summary>
        public int CommunityId
        {
            get;
            set;
        }
        /// <summary>
        /// 社区名称
        /// </summary>
        public string CommunityName
        {
            get;
            set;
        }
    }
}
