﻿using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 社区dto
    /// </summary>
    public class CommunityDto: DtoBase
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 社区名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress
        {
            get;
            set;
        }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }
    }
}
