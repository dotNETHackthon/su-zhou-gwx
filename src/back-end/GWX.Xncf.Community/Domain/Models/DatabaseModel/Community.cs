﻿using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel
{
    /// <summary>
    /// 社区
    /// </summary>
    [Table($"{Register.DATABASE_PREFIX}{nameof(Community)}")]
    [Serializable]
    public class Community : EntityBase<int>
    {
        /// <summary>
        /// 私有函数
        /// </summary>
        private Community()
        {
        }

        /// <summary>
        /// 社区名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress
        {
            get;
            private set;
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        [StringLength(50)]
        public string Phone { get; private set; }

        /// <summary>
        /// 公有构造函数
        /// </summary>
        /// <param name="name">社区名称</param>
        /// <param name="detailedAddress">社区详细地址</param>
        /// <param name="phone">社区电话</param>
        public Community(string name, string detailedAddress = "", string phone = "")
        {
            this.Name = name;
            this.DetailedAddress = detailedAddress;
            this.Phone = phone;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="vaccineName">疫苗名称</param>
        /// <param name="vaccinesId">疫苗编号</param>
        /// <param name="batchNo">批次号</param>
        internal void Update(int id, string name, string detailedAddress = "", string phone = "")
        {
            this.Id = id;
            this.Name = name;
            this.DetailedAddress = detailedAddress;
            this.Phone = phone;
        }
    }

}
