﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel.Mapping
{
    public class Community_ConfigurationMapping: ConfigurationMappingWithIdBase<Community, int>
    {
        public override void Configure(EntityTypeBuilder<Community> builder)
        {
        }
    }
}
