﻿using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Models.DatabaseModel
{
    /// <summary>
    /// 接种点表
    /// </summary>
    [Table($"{Register.DATABASE_PREFIX}{nameof(VaccineRash)}")]
    [Serializable]
    public class VaccineRash : EntityBase<int>
    {
        /// <summary>
        /// 私有构造函数
        /// </summary>
        private VaccineRash()
        {
        }
        /// <summary>
        /// 接种点名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address
        {
            get;
            private set;
        }
        /// <summary>
        /// 联系方式
        /// </summary>
        [StringLength(50)]
        public string Phone
        {
            get;
            private set;
        }
        /// <summary>
        /// 社区id
        /// </summary>
        [StringLength(50)]
        public int CommunityId
        {
            get;
            private set;
        }
        /// <summary>
        /// 社区名称
        /// </summary>
        [StringLength(50)]
        public string CommunityName
        {
            get;
            private set;
        }
        /// <summary>
        /// 公有构造函数
        /// </summary>
        /// <param name="name">接种点名称</param>
        public VaccineRash(string name, int communityId = 0, string communityName = "")
        {
            this.Name = name;
            this.CommunityId = communityId;
            this.CommunityName = communityName;
        }
    }
}
