﻿using AutoMapper;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Services
{
    /// <summary>
    /// 社区service
    /// </summary>
    public class CommunityService : ServiceBase<GWX.Xncf.Community.Domain.Models.DatabaseModel.Community>
    {
        private readonly IMapper _mapper;
        public CommunityService(Senparc.Ncf.Repository.IRepositoryBase<GWX.Xncf.Community.Domain.Models.DatabaseModel.Community> repo, IServiceProvider serviceProvider, IMapper mapper) : base(repo, serviceProvider)
        {
            this._mapper = mapper;
        }
        /// <summary>
        /// 创建社区
        /// </summary>
        /// <param name="dto">社区dto</param>
        /// <returns></returns>
        public async Task CreateOrUpdateAsync(CommunityDto dto)
        {
            if (string.IsNullOrEmpty(dto.Id.ToString()))
            {
                var res = _mapper.Map<GWX.Xncf.Community.Domain.Models.DatabaseModel.Community>(dto);
                await SaveObjectAsync(res);

            }
            else
            {
                var obj = GetObject(z => z.Id == dto.Id);
                if (obj == null)
                {
                    return;
                }
                obj.Update(dto.Id, dto.Name, dto.DetailedAddress, dto.Phone);
                await SaveObjectAsync(obj);
            }
        }

        /// <summary>
        /// 根据编号删除社区
        /// </summary>
        /// <param name="vaccineId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(int vaccineId)
        {
            var obj = GetObject(z => z.Id == vaccineId);
            if (obj != null)
            {
                await DeleteObjectAsync(obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistAsync(int id)
        {
            return GetCount(z => z.Id == id) > 0;
        }

    }
}
