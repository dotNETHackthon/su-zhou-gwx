﻿using AutoMapper;
using GWX.Xncf.Community.Domain.Models.DatabaseModel;
using GWX.Xncf.Community.Domain.Models.DatabaseModel.Dto;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;

namespace GWX.Xncf.Community.Domain.Services
{
    /// <summary>
    /// 接种点服务
    /// </summary>
    public class VaccineRashService : ServiceBase<VaccineRash>
    {
        private IMapper _mapper;

        public VaccineRashService(IRepositoryBase<VaccineRash> repo, IServiceProvider serviceProvider, IMapper mapper) : base(repo, serviceProvider)
        {
            this._mapper = mapper;
        }

        /// <summary>
        /// 创建接种点
        /// </summary>
        /// <param name="dto">社区dto</param>
        /// <returns></returns>
        public async Task CreateAsync(VaccineRashDto dto)
        {
            var res = _mapper.Map<VaccineRash>(dto);
            await SaveObjectAsync(res);
        }

        /// <summary>
        /// 根据编号删除接种点
        /// </summary>
        /// <param name="vaccineId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(int vaccineId)
        {
            var obj = GetObject(z => z.Id == vaccineId);
            if (obj != null)
            {
                await DeleteObjectAsync(obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistAsync(int id)
        {
            return GetCount(z => z.Id == id) > 0;
        }
    }
}