﻿using Senparc.Ncf.Core.AppServices;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Microsoft.AspNetCore.Mvc;
using Gwx.Xncf.Vaccines.OHS.Local.PL;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;
using Senparc.CO2NET;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Senparc.Ncf.Core.Models;
using System.Linq;
using Senparc.Ncf.Utility;
using Senparc.CO2NET.Extensions;

namespace Gwx.Xncf.Vaccines.OHS.Local.AppService
{
    public class BatchVaccineAppService : AppServiceBase
    {
        private BatchVaccineService _batchVaccineService;
        private VaccineService _vaccineService;
        private IMapper _mapper;

        public BatchVaccineAppService(BatchVaccineService batchVaccineService, IServiceProvider serviceProvider,
            IMapper mapper, VaccineService vaccineService) : base(serviceProvider)
        {
            _batchVaccineService = batchVaccineService;
            _mapper = mapper;
            _vaccineService = vaccineService;
        }

        /// <summary>
        /// 添加疫苗信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<BatchVaccine_CreateResponse>> Create(BatchVaccine_CreateRequest request)
        {
            var result =
                await this.GetResponseAsync<AppResponseBase<BatchVaccine_CreateResponse>, BatchVaccine_CreateResponse>(
                    async (response, logger) =>
                    {
                        if (!_vaccineService.ExistAsync(request.VaccineId))
                        {
                            throw new AggregateException("疫苗编号不存在");
                        }

                        await _batchVaccineService.CreateAsync(request.BatchNo, request.VaccineId, request.VaccineName,
                            request.Qty);
                        return new BatchVaccine_CreateResponse();
                    });
            return result;
        }

        /// <summary>
        /// 添加疫苗批次信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Put)]
        public async Task<AppResponseBase<BatchVaccine_CreateResponse>> Update([FromQuery] int id,
            BatchVaccine_UpdateRequest request)
        {
            var result =
                await this.GetResponseAsync<AppResponseBase<BatchVaccine_CreateResponse>, BatchVaccine_CreateResponse>(
                    async (response, logger) =>
                    {
                        if (!_vaccineService.ExistAsync(request.VaccineId))
                        {
                            throw new AggregateException("疫苗编号不存在");
                        }

                        await _batchVaccineService.UpdateAsync(id,
                            new BatchVaccineDto()
                            {
                                BatchNo = request.BatchNo, VaccineName = request.VaccineName,
                                VaccinesId = request.VaccineId
                            });
                        return new BatchVaccine_CreateResponse();
                    });
            return result;
        }

        /// <summary>
        /// 删除疫苗批次
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<bool>> Delete(int id)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _batchVaccineService.DeleteByIdAsync(id);
                return result;
            });
            return result;
        }

        /// <summary>
        /// 查询疫苗批次
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<BatchVaccine_GetListResponse>> Get(int pageIndex, int pageSize = 10,
            string search = "")
        {
            var result =
                await this
                    .GetResponseAsync<AppResponseBase<BatchVaccine_GetListResponse>, BatchVaccine_GetListResponse>(
                        async (response, logger) =>
                        {
                            var seh = new SenparcExpressionHelper<BatchVaccine>();
                            seh.ValueCompare.AndAlso(!search.IsNullOrEmpty(), z => z.BatchNo.Contains(search));
                            var where = seh.BuildWhereExpression();

                            var list = await _batchVaccineService.GetObjectListAsync(pageIndex, pageSize, where,
                                z => z.Id, Senparc.Ncf.Core.Enums.OrderingType.Descending);
                            var dtoVaccines = _mapper.Map<List<BatchVaccineDto>>(list);
                            return new BatchVaccine_GetListResponse()
                            {
                                List = dtoVaccines,
                                TotalCount = list.TotalCount
                            };
                        });
            return result;
        }

        /// <summary>
        /// 获取批次信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<BatchVaccineDto> GetBatchVaccineById(int Id)
        {
            var data = await _batchVaccineService.GetObjectAsync(s => s.Id == Id);
            var result = _mapper.Map<BatchVaccineDto>(data);
            return result;
        }
    }
}