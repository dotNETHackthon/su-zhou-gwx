﻿using Senparc.Ncf.Core.AppServices;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Microsoft.AspNetCore.Mvc;
using Gwx.Xncf.Vaccines.OHS.Local.PL;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;
using Senparc.CO2NET;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Senparc.Ncf.Core.Models;
using System.Linq;
using Senparc.Ncf.Utility;
using Senparc.CO2NET.Extensions;

namespace Gwx.Xncf.Vaccines.OHS.Local.AppService
{
    public class PlanVaccineAppService : AppServiceBase
    {
        private PlanVaccineService _planVaccineService;
        private BatchVaccineService _batchVaccineService;
        private VaccineService _vaccineService;
        private IMapper _mapper;

        public PlanVaccineAppService(BatchVaccineService batchVaccineService, IServiceProvider serviceProvider,
            IMapper mapper, VaccineService vaccineService, PlanVaccineService planVaccineService,
            BatchVaccineService batchVaccineService1) : base(serviceProvider)
        {
            _mapper = mapper;
            _vaccineService = vaccineService;
            _planVaccineService = planVaccineService;
            _batchVaccineService = batchVaccineService;
        }

        /// <summary>
        /// 添加接种计划
        /// </summary>
        /// <param name="request">添加计划</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<PlanVaccine_CreateResponse>> Create(PlanVaccine_CreateRequest request)
        {
            var result =
                await this.GetResponseAsync<AppResponseBase<PlanVaccine_CreateResponse>, PlanVaccine_CreateResponse>(
                    async (response, logger) =>
                    {
                        await _planVaccineService.CreateAsync(request.Name,request.BatchId,request.Qty,request.InoculationpointId,request.PlanStartDate,request.PlanEndDate);
                        return new PlanVaccine_CreateResponse();
                    });
            return result;
        }

        /// <summary>
        /// 更近计划
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="request">内容</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Put)]
        public async Task<AppResponseBase<PlanVaccine_CreateResponse>> Update([FromQuery] int id,
            PlanVaccine_CreateRequest request)
        {
            var result =
                await this.GetResponseAsync<AppResponseBase<PlanVaccine_CreateResponse>, PlanVaccine_CreateResponse>(
                    async (response, logger) =>
                    {
                        await _planVaccineService.UpdateAsync(id,
                            new PlanVaccineDto()
                            {
                                Name = request.Name, PlanStartDate = request.PlanStartDate,
                                InoculationpointId = request.InoculationpointId,
                                PlanEndDate = request.PlanEndDate, BatchId = request.BatchId, Qty = request.Qty
                            });
                        return new PlanVaccine_CreateResponse();
                    });
            return result;
        }

        /// <summary>
        /// 删除计划
        /// </summary>
        /// <param name="id">计划编号</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Delete)]
        public async Task<AppResponseBase<bool>> Delete(int id)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _batchVaccineService.DeleteByIdAsync(id);
                return result;
            });
            return result;
        }

        /// <summary>
        /// 查询计划
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pointId"></param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<PlanVaccine_GetListResponse>> Get(int pageIndex, int pageSize = 10,
            int? pointId = null)
        {
            var result =
                await this.GetResponseAsync<AppResponseBase<PlanVaccine_GetListResponse>, PlanVaccine_GetListResponse>(
                    async (response, logger) =>
                    {
                        var seh = new SenparcExpressionHelper<PlanVaccine>();
                        seh.ValueCompare.AndAlso(pointId.HasValue, z => z.InoculationpointId == pointId);
                        var where = seh.BuildWhereExpression();

                        var list = await _planVaccineService.GetObjectListAsync(pageIndex, pageSize, where, z => z.Id,
                            Senparc.Ncf.Core.Enums.OrderingType.Descending);
                        var dtoVaccines = _mapper.Map<List<PlanVaccineDto>>(list);
                        return new PlanVaccine_GetListResponse()
                        {
                            List = dtoVaccines,
                            TotalCount = list.TotalCount
                        };
                    });
            return result;
        }
    }
}