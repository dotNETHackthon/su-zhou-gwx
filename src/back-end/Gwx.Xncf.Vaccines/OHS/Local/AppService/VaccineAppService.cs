﻿using Senparc.Ncf.Core.AppServices;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Microsoft.AspNetCore.Mvc;
using Gwx.Xncf.Vaccines.OHS.Local.PL;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;
using Senparc.CO2NET;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Senparc.Ncf.Core.Models;
using System.Linq;
using Senparc.Ncf.Utility;
using Senparc.CO2NET.Extensions;

namespace Gwx.Xncf.Vaccines.OHS.Local.AppService
{
    public class VaccineAppService : AppServiceBase
    {
        private VaccineService _vaccineService;
        private IMapper _mapper;

        public VaccineAppService(VaccineService vaccineService, IServiceProvider serviceProvider, IMapper mapper) : base(serviceProvider)
        {
            _vaccineService = vaccineService;
            _mapper = mapper;
        }

        /// <summary>
        /// 添加疫苗信息
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Post)]
        public async Task<AppResponseBase<Vaccine_CreateResponse>> Create(Vaccine_CreateRequest request)
        {
            var result = await this.GetResponseAsync<AppResponseBase<Vaccine_CreateResponse>, Vaccine_CreateResponse>(async (response, logger) =>
             {
                 await _vaccineService.CreateAsync(request.Name, request.Type, request.SupplierName, request.SupplierPhone, request.SupplierAddress);
                 return new Vaccine_CreateResponse();
             });
            return result;
        }
        /// <summary>
        /// 删除疫苗
        /// </summary>  
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<bool>> Delete(int id)
        {
            var result = await this.GetResponseAsync<AppResponseBase<bool>, bool>(async (response, logger) =>
            {
                var result = await _vaccineService.DeleteByIdAsync(id);
                return true;
            });
            return result;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="request">疫苗信息</param>
        /// <returns></returns>
        [ApiBind(ApiRequestMethod = Senparc.CO2NET.WebApi.ApiRequestMethod.Get)]
        public async Task<AppResponseBase<Vaccine_GetListResponse>> Get(int pageIndex = 1, int pageSize = 20, string search = "")
        {
            var result = await this.GetResponseAsync<AppResponseBase<Vaccine_GetListResponse>, Vaccine_GetListResponse>(async (response, logger) =>
            {
                var seh = new SenparcExpressionHelper<Vaccine>();
                seh.ValueCompare.AndAlso(!search.IsNullOrEmpty(), z => z.Name.Contains(search));
                var where = seh.BuildWhereExpression();

                var list = await _vaccineService.GetObjectListAsync(pageIndex, pageSize, where, z => z.Id, Senparc.Ncf.Core.Enums.OrderingType.Descending);
                var dtoVaccines = _mapper.Map<List<VaccineDto>>(list);

                return new Vaccine_GetListResponse()
                {
                    List = dtoVaccines,
                    TotalCount = list.TotalCount
                };
            });
            return result;
        }
    }
}
