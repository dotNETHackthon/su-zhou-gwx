﻿using System;
namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    /// <summary>
    /// 创建疫苗批次
    /// </summary>
    public class BatchVaccine_CreateRequest
    {
        /// <summary>
        /// 疫苗编号
        /// </summary>
        public int VaccineId { get; set; }

        /// <summary>
        /// 批次编号
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// 批次数量
        /// </summary>
        public int Qty { get; set; }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        public string VaccineName { get; set; }
    }

    /// <summary>
    /// 编辑疫苗批次
    /// </summary>
    public class BatchVaccine_UpdateRequest
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 疫苗编号
        /// </summary>
        public int VaccineId { get; set; }

        /// <summary>
        /// 批次编号
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// 批次数量
        /// </summary>
        public int Qty { get; set; }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        public string VaccineName { get; set; }
    }



    /// <summary>
    /// 删除疫苗
    /// </summary>
    public class BatchVaccine_DeleteRequest
    {
        public int VaccineId { get; set; }
    }

    /// <summary>
    /// 列表查询
    /// </summary>
    public class BatchVaccine_GetRequest
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Search { get; set; }
    }
}

