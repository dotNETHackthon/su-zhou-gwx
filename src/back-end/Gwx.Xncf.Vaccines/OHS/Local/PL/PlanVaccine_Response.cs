﻿using System;
using System.Collections.Generic;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;

namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanVaccine_CreateResponse
    {

    }

    /// <summary>
    /// 接种计划列表
    /// </summary>
    public class PlanVaccine_GetListResponse
    {
        public List<PlanVaccineDto> List { get; set; }
        public int TotalCount { get; set; }
    }
}

