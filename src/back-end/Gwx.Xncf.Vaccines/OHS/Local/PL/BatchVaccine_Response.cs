﻿using System;
using System.Collections.Generic;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;

namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    /// <summary>
    /// 
    /// </summary>
    public class BatchVaccine_CreateResponse
    {

    }

    /// <summary>
    /// 疫苗批次列表
    /// </summary>
    public class BatchVaccine_GetListResponse
    {
        public List<BatchVaccineDto> List { get; set; }
        public int TotalCount { get; set; }
    }
}

