﻿using System;
namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    /// <summary>
    /// 创建接种计划
    /// </summary>
    public class PlanVaccine_CreateRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 批次编号
        /// </summary>
        public int BatchId
        {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty
        {
            get;
            set;
        }

        /// <summary>
        /// 接种点
        /// </summary>
        public int InoculationpointId
        {
            get;
            set;
        }

        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime PlanStartDate
        {
            get;
            set;
        }

        /// <summary>
        /// 计划结束时间
        /// </summary>
        public DateTime PlanEndDate
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 列表查询
    /// </summary>
    public class PlanVaccine_GetRequest
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Search { get; set; }
    }
}

