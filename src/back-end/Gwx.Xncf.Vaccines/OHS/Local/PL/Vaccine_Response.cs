﻿using System;
using System.Collections.Generic;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;

namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    public class Vaccine_CreateResponse
    {

    }

    /// <summary>
    /// 疫苗列表
    /// </summary>
    public class Vaccine_GetListResponse
    {
        public List<VaccineDto> List { get; set; }
        public int TotalCount { get; set; }
    }
}

