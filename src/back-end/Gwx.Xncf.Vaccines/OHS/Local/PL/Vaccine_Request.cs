﻿using System;
namespace Gwx.Xncf.Vaccines.OHS.Local.PL
{
    public class Vaccine_CreateRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public short Type { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商手机号
        /// </summary>
        public string SupplierPhone
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商地址
        /// </summary>
        public string SupplierAddress
        {
            get;
            set;
        }
    }


    /// <summary>
    /// 删除疫苗
    /// </summary>
    public class Vaccine_DeleteRequest
    {
        public int VaccineId { get; set; }
    }

    /// <summary>
    /// 列表查询
    /// </summary>
    public class Vaccine_GetRequest
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Search { get; set; }
    }
}

