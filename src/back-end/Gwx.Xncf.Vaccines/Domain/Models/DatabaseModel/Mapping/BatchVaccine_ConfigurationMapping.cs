﻿using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace Gwx.Xncf.Vaccines.Models
{
    [XncfAutoConfigurationMapping]
    public class Vaccines_ColorConfigurationMapping : ConfigurationMappingWithIdBase<BatchVaccine, int>
    {
        public override void Configure(EntityTypeBuilder<BatchVaccine> builder)
        {
        }
    }
}
