﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel
{
    /// <summary>
    /// 疫苗批次
    /// </summary>
    [Table($"{Register.DATABASE_PREFIX}{nameof(BatchVaccine)}")]
    [Serializable]
    public class BatchVaccine : EntityBase<int>
    {
        private BatchVaccine()
        {
        }

        /// <summary>
        /// 批次号
        /// </summary>
        [Required]
        [StringLength(50)]
        [ConcurrencyCheck]
        public string BatchNo
        {
            get;
            private set;
        }

        /// <summary>
        /// 疫苗编号
        /// </summary>
        [Required]
        public int VaccinesId
        {
            get;
            private set;
        }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        public string VaccineName
        {
            get;
            private set;
        }

        /// <summary>
        /// 批次库存
        /// </summary>
        [Required]
        public int Qty
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchNo">批次号</param>
        /// <param name="vaccinesId">疫苗编号</param>
        /// <param name="vaccineName">疫苗名称</param>
        /// <param name="qty">库存</param>
        public BatchVaccine(string batchNo, int vaccinesId, string vaccineName, int qty)
        {
            BatchNo = batchNo;
            VaccinesId = vaccinesId;
            VaccineName = vaccineName;
            Qty = qty;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="vaccineName">疫苗名称</param>
        /// <param name="vaccinesId">疫苗编号</param>
        /// <param name="batchNo">批次号</param>
        internal void Update(string vaccineName, int vaccinesId, string batchNo)
        {
            VaccineName = vaccineName;
            VaccinesId = vaccinesId;
            BatchNo = batchNo;
        }
    }
}

