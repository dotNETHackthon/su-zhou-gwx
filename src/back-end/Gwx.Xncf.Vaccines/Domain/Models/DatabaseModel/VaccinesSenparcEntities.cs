﻿using Microsoft.EntityFrameworkCore;
using Senparc.Ncf.Database;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.XncfBase.Database;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;

namespace Gwx.Xncf.Vaccines.Models
{
    public class VaccinesSenparcEntities : XncfDatabaseDbContext
    {
        public VaccinesSenparcEntities(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
        }

        /// <summary>
        /// 疫苗
        /// </summary>
        public DbSet<BatchVaccine> BatchVaccines { get; set; }

        /// <summary>
        /// 批次记录
        /// </summary>
        public DbSet<Vaccine> Vaccines { get; set; }

        /// <summary>
        /// 接种计划
        /// </summary>
        public DbSet<PlanVaccine> PlanVaccines { get; set; }

        //DOT REMOVE OR MODIFY THIS LINE 请勿移除或修改本行 - Entities Point
        //ex. public DbSet<Color> Colors { get; set; }

        //如无特殊需需要，OnModelCreating 方法可以不用写，已经在 Register 中要求注册
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //}
    }
}
