﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel
{
    /// <summary>
    /// 疫苗
    /// </summary>
    [Table($"{Register.DATABASE_PREFIX}{nameof(Vaccine)}")]
    [Serializable]
    public class Vaccine : EntityBase<int>
    {
        private Vaccine()
        {
        }

        /// <summary>
        /// 名称
        /// </summary>
        [StringLength(50)]
        public string Name { get; private set; }

        /// <summary>
        /// 类型
        /// </summary>
        public short Type { get; private set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        [StringLength(50)]
        public string SupplierName
        {
            get;
            private set;
        }

        /// <summary>
        /// 供应商手机号
        /// </summary>
        [StringLength(20)]
        public string SupplierPhone
        {
            get;
            private set;
        }

        /// <summary>
        /// 供应商地址
        /// </summary>
        [StringLength(200)]
        public string SupplierAddress
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="type">类型</param>
        /// <param name="supplierName">供应商名称</param>
        /// <param name="supplierPhone">供应商手机号</param>
        /// <param name="supplierAddress">供应商地址</param>
        public Vaccine(string name, short type, string supplierName, string supplierPhone, string supplierAddress)
        {
            Name = name;
            Type = type;
            SupplierName = supplierName;
            SupplierPhone = supplierPhone;
            SupplierAddress = supplierAddress;
        }


    }
}

