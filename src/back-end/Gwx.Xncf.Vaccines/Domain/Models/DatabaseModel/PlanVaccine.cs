﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel
{
    /// <summary>
    /// 接种计划
    /// </summary>
    [Table($"{Register.DATABASE_PREFIX}{nameof(PlanVaccine)}")]
    [Serializable]
    public class PlanVaccine : EntityBase<int>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 批次编号
        /// </summary>
        public int BatchId { get; private set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; private set; }

        /// <summary>
        /// 接种点
        /// </summary>
        public int InoculationpointId { get; private set; }

        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime PlanStartDate { get; private set; }

        /// <summary>
        /// 计划结束时间
        /// </summary>
        public DateTime PlanEndDate { get; private set; }

        private PlanVaccine()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="batchId"></param>
        /// <param name="qty"></param>
        /// <param name="inoculationpointId"></param>
        /// <param name="planStartDate"></param>
        /// <param name="planEndDate"></param>
        public PlanVaccine(string name, int batchId, int qty, int inoculationpointId, DateTime planStartDate,
            DateTime planEndDate)
        {
            Name = name;
            BatchId = batchId;
            Qty = qty;
            InoculationpointId = inoculationpointId;
            PlanStartDate = planStartDate;
            PlanEndDate = planEndDate;
        }

        /// <summary>
        /// 更新接种计划
        /// </summary>
        /// <param name="name"></param>
        /// <param name="qty"></param>
        /// <param name="batchId"></param>
        /// <param name="inoculationpointId"></param>
        /// <param name="planStartDate"></param>
        /// <param name="planEndDate"></param>
        public void Update(string name, int qty, int batchId, int inoculationpointId, DateTime planStartDate,
            DateTime planEndDate)
        {
            Name = name;
            Qty = qty;
            BatchId = batchId;
            InoculationpointId = inoculationpointId;
            PlanStartDate = planStartDate;
            PlanEndDate = planEndDate;
        }
    }
}