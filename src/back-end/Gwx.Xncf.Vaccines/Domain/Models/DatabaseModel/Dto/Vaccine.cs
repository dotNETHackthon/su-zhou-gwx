﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 疫苗
    /// </summary>
    public class VaccineDto : DtoBase
    {
        public int Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public short Type { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商手机号
        /// </summary>
        public string SupplierPhone
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商地址
        /// </summary>
        public string SupplierAddress
        {
            get;
            set;
        }

        /// <summary>
        /// 生成疫苗对象
        /// </summary>
        /// <returns></returns>
        public Vaccine ToVaccine()
        {
            return new Vaccine(Name, Type, SupplierName, SupplierPhone, SupplierAddress);
        }
    }
}

