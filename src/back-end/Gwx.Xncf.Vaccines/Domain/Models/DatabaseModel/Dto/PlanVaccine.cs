﻿using System;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 接种计划
    /// </summary>
    public class PlanVaccineDto : DtoBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 批次编号
        /// </summary>
        public int BatchId
        {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty
        {
            get;
            set;
        }

        /// <summary>
        /// 接种点
        /// </summary>
        public int InoculationpointId
        {
            get;
            set;
        }

        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime PlanStartDate
        {
            get;
            set;
        }

        /// <summary>
        /// 计划结束时间
        /// </summary>
        public DateTime PlanEndDate
        {
            get;
            set;
        }
    }
}

