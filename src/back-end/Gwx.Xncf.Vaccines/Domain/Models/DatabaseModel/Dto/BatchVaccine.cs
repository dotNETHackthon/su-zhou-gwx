﻿using System;
using System.ComponentModel.DataAnnotations;
using Senparc.Ncf.Core.Models;

namespace Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 疫苗批次
    /// </summary>
    public class BatchVaccineDto : DtoBase
    {

        /// <summary>
        /// 批次号
        /// </summary>
        public string BatchNo
        {
            get;
            set;
        }

        /// <summary>
        /// 疫苗编号
        /// </summary>
        public int VaccinesId
        {
            get;
            set;
        }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        public string VaccineName
        {
            get;
            set;
        }

        public int Qty
        {
            get;
            set;
        }
    }
}

