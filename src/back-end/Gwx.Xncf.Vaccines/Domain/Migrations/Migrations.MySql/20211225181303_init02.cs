﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Gwx.Xncf.Vaccines.Domain.Migrations.Migrations.MySql
{
    public partial class init02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Qty",
                table: "Gwx_Vaccines_BatchVaccine",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Qty",
                table: "Gwx_Vaccines_BatchVaccine");
        }
    }
}
