﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Gwx.Xncf.Vaccines.Domain.Migrations.Migrations.MySql
{
    public partial class init01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_BatchVaccines",
                table: "BatchVaccines");

            migrationBuilder.RenameTable(
                name: "BatchVaccines",
                newName: "Gwx_Vaccines_BatchVaccine");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Gwx_Vaccines_BatchVaccine",
                table: "Gwx_Vaccines_BatchVaccine",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Gwx_Vaccines_BatchVaccine",
                table: "Gwx_Vaccines_BatchVaccine");

            migrationBuilder.RenameTable(
                name: "Gwx_Vaccines_BatchVaccine",
                newName: "BatchVaccines");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BatchVaccines",
                table: "BatchVaccines",
                column: "Id");
        }
    }
}
