﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;

namespace Gwx.Xncf.Vaccines.Domain.Services
{
    /// <summary>
    /// 接种计划
    /// </summary>
    public class PlanVaccineService : ServiceBase<PlanVaccine>
    {
        private readonly IMapper _mapper;

        public PlanVaccineService(IRepositoryBase<PlanVaccine> repo, IMapper mapper, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// 创建疫苗批次信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task CreateAsync(PlanVaccineDto dto)
        {
            var vaccine = _mapper.Map<PlanVaccine>(dto);
            await SaveObjectAsync(vaccine);
        }

        /// <summary>
        /// 编辑疫苗批次信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task UpdateAsync(int id, PlanVaccineDto dto)
        {
            var obj = GetObject(z => z.Id == id);
            if (obj == null)
            {
                throw new AggregateException("接种计划不存在");
            }
            obj.Update(dto.Name, dto.Qty,dto.BatchId,dto.InoculationpointId,dto.PlanStartDate,dto.PlanEndDate);
            await SaveObjectAsync(obj);
        }

        /// <summary>
        /// 根据编号删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(int id)
        {
            var obj = GetObject(z => z.Id == id);
            if (obj != null)
            {
                await DeleteObjectAsync(obj);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="batchId"></param>
        /// <param name="qty"></param>
        /// <param name="inoculationpointId"></param>
        /// <param name="planStartDate"></param>
        /// <param name="planEndDate"></param>
        /// <exception cref="AggregateException"></exception>
        public async Task CreateAsync(string name, int batchId, int qty, int inoculationpointId, DateTime planStartDate,
            DateTime planEndDate)
        {
            if (qty <= 0)
            {
                throw new AggregateException("库存数量必须要大于0");
            }

            var obj = new PlanVaccine(name, batchId, qty, inoculationpointId, planStartDate, planEndDate);
            await SaveObjectAsync(obj);
        }
    }
}