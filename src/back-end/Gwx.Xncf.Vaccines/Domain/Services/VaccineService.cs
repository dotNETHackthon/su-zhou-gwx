﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;

namespace Gwx.Xncf.Vaccines.Domain.Services
{
    /// <summary>
    /// 疫苗
    /// </summary>
    public class VaccineService : ServiceBase<Vaccine>
    {
        private readonly IMapper _mapper;
        public VaccineService(IRepositoryBase<Vaccine> repo, IMapper mapper, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// 创建疫苗信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<VaccineDto> CreateAsync(VaccineDto dto)
        {
            var vaccine = dto.ToVaccine();
            await SaveObjectAsync(vaccine);
            return dto;
        }

        /// <summary>
        /// 根据编号删除数据
        /// </summary>
        /// <param name="vaccineId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(int vaccineId)
        {
            var obj = GetObject(z => z.Id == vaccineId);
            if (obj != null)
            {
                await DeleteObjectAsync(obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 创建疫苗
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="supplierName"></param>
        /// <param name="supplierPhone"></param>
        /// <param name="supplierAddress"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task CreateAsync(string name, short type, string supplierName, string supplierPhone, string supplierAddress)
        {
            await CreateAsync(new VaccineDto() { Name = name, SupplierName = supplierName, SupplierPhone = supplierPhone, SupplierAddress = supplierAddress });
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="vaccineId"></param>
        /// <returns></returns>
        public bool ExistAsync(int id)
        {
            return GetCount(z => z.Id == id) > 0;
        }
    }
}
