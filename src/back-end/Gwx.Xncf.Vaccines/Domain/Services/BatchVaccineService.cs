﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using Gwx.Xncf.Vaccines.Domain.Services;
using System;
using System.Threading.Tasks;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel;
using Gwx.Xncf.Vaccines.Domain.Models.DatabaseModel.Dto;
using AutoMapper;

namespace Gwx.Xncf.Vaccines.Domain.Services
{
    /// <summary>
    /// 疫苗
    /// </summary>
    public class BatchVaccineService : ServiceBase<BatchVaccine>
    {
        private readonly IMapper _mapper;
        public BatchVaccineService(IRepositoryBase<BatchVaccine> repo, IMapper mapper, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// 创建疫苗批次信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task CreateAsync(BatchVaccineDto dto)
        {
            var vaccine = _mapper.Map<BatchVaccine>(dto);
            await SaveObjectAsync(vaccine);
        }

        /// <summary>
        /// 编辑疫苗批次信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task UpdateAsync(int id, BatchVaccineDto dto)
        {
            var obj = GetObject(z => z.Id == id);
            if (obj == null)
            {
                throw new AggregateException("批次不存在");
            }
            if (ExistBatch(dto.BatchNo, obj.Id))
            {
                throw new AggregateException("批次号已存在");
            }

            obj.Update(dto.VaccineName, dto.VaccinesId, dto.BatchNo);
            await SaveObjectAsync(obj);
        }


        /// <summary>
        /// 根据编号删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdAsync(int id)
        {
            var obj = GetObject(z => z.Id == id);
            if (obj != null)
            {
                await DeleteObjectAsync(obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 创建批次
        /// </summary>
        /// <param name="batchNo">批次号</param>
        /// <param name="vaccineId">疫苗编号</param>
        /// <param name="vaccineName">疫苗名称</param>
        /// <param name="qty">数量</param>
        /// <returns></returns>
        public async Task CreateAsync(string batchNo, int vaccineId, string vaccineName, int qty)
        {
            if (qty <= 0)
            {
                throw new AggregateException("库存数量必须要大于0");
            }
            if (ExistBatch(batchNo))
            {
                throw new AggregateException("批次号禁止重复");
            }
            var obj = new BatchVaccine(batchNo, vaccineId, vaccineName, qty);
            await SaveObjectAsync(obj);
        }


        /// <summary>
        /// 批次号是否存在
        /// </summary>
        /// <param name="batchNo"></param>
        /// <returns></returns>
        public bool ExistBatch(string batchNo) => GetCount(z => z.BatchNo == batchNo) > 0;

        /// <summary>
        /// 批次号是否存在
        /// </summary>
        /// <param name="batchNo">批次号</param>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public bool ExistBatch(string batchNo, int id)
        {
            var obj = GetObject(z => z.BatchNo == batchNo);
            return obj.Id != id;
        }

    }
}
